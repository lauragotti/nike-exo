import Link from "next/link";
import React from "react";
import { Inter } from "next/font/google";

const inter = Inter({
  subsets: ["latin"],
  weight: ["200", "300", "400", "500"],
});

function HomePageView() {
  return (
    <div
      className={` ${inter.className}  w-screen min-h-screen flex flex-col flex-1 bg-zinc-900 items-center justify-center `}
    >
      <div className=" relative flex flex-row p-5 bg-gradient-to-r  from-zinc-800  to-zinc-600 w-6/12 rounded-md shadow-xl overflow-visible">
        <div className=" w-1/2  flex align-middle ">
          <img
            src="/nike.png"
            className="-scale-x-100 origin-center rotate-12 absolute -left-20 -top-10  max-w-[500px] "
          />
        </div>
        <div className="flex-col w-1/2 ">
          <p className="text-2xl font-bold bg-gradient-to-r from-orange-600 via-orange-100 to-orange-600 text-transparent  bg-clip-text">
            NIKE
          </p>
          <p className="text-white font-extrabold text-4xl ps-3">FLYKNIT</p>
          <p className="uppercase text-xs  font-bold text-white mt-10">
            product overview
          </p>
          <p className="text-xs text-zinc-400 ps-3">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa hic
            esse voluptatum! Libero, necessitatibus illo ad eaque commodi eius
            id repellendus quam tempore porro sed dolore sapiente animi vero
            natus?
          </p>
          <div className="flex mt-4 gap-2 ps-3 text-xs text-white">
            <button className="w-8 h-8  rounded-md bg-zinc-600 hover:from-orange-400 hover:to-orange-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              7
            </button>
            <button className="w-8 h-8  rounded-md  bg-zinc-600 hover:from-orange-400 hover:to-orange-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              8
            </button>
            <button className="w-8 h-8  rounded-md  bg-zinc-600 hover:from-orange-400 hover:to-orange-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              9
            </button>
            <button className="w-8 h-8  rounded-md  bg-zinc-600 hover:from-orange-400 hover:to-orange-500 hover:bg-gradient-to-r transition duration-300 shadow-md">
              10
            </button>
          </div>

          <div className="text-white font-bold flex gap-10 mt-10 items-end justify-end">
            <p style={{ textShadow: "-20px -10px 1px rgba(206,206,206,0.4)" }}>
              $120
            </p>
            <button className="px-6 py-2 text-xs   rounded-md  bg-gradient-to-r from-orange-400 to-orange-500 hover:from-orange-300  hover:to-orange-400 hover:bg-gradient-to-r transition duration-300 shadow-md">
              PURCHASE
            </button>
          </div>
        </div>
      </div>
      <div></div>
    </div>
  );
}

export default HomePageView;
