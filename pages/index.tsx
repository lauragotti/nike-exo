import Image from "next/image";
import { Inter } from "next/font/google";
import HomePageView from "@/src/view/HomePageView";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <HomePageView />
    </>
  );
}
